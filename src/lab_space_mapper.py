'''
Author: Lincong Li
Date: July 5th 2016
'''

from wsgiref.simple_server import make_server
import sys
import time
from threading import Thread
import subprocess
from pprint import pprint

GREEN = 1
GREY = 2
GREEN_RGB_TUPLE = (105, 245, 54)  # RGB values of green
GREY_RGB_TUPLE = (130, 130, 130)  # RGB values of grey
RED_REG_TUPLE = (255, 0, 0)  # RGB values of red

DEFAUT_OUT_LINE = 'USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT'
CREATE_STATION_JS_LINE = 'rects.push(new rect("station %s ", %s, %s, %s, %s, "rgb(%s, %s, %s)", %s, %s, %s, "black",3));'

port_num = 9080

def get_time_str():
    time_struct = time.localtime()
    return str(time_struct.tm_hour) + ':' + str(time_struct.tm_min) + ':' + str(time_struct.tm_sec)


# initializing global data structure that share information between the front-end and back-end
lab_space_info_dict = {}
for i in range(0, 40):
    lab_space_info_dict[i+1] = [False, get_time_str()]


pprint(lab_space_info_dict)

def get_color_tuple(option):
    if option is GREEN:
        return GREEN_RGB_TUPLE

    elif option is GREY:
        return GREY_RGB_TUPLE

    else: # error case, return red
        return RED_REG_TUPLE


def generate_html(room_start_x, room_start_y):

    fh = open("lab_mapping.html")
    line = fh.readline()
    html = line

    while line:
        line = fh.readline()
        html += line
        if CREATE_STATION_JS_LINE in line:
            for i in range(0, 39):
                html += line

    station_size = 55
    room_width = 450
    room_length = 650
    para_tuples = (room_start_x, room_start_y, room_width, room_length)

    total_row = 10
    total_col = 4
    lab_station_index = 1

    for j in range(0 ,total_col):
        if j is 0:
            station_x = room_start_x + room_width - station_size - 3
        elif j is 1:
            station_x = room_start_x + room_width - station_size - 3 - 150
        elif j is 2:
            station_x = room_start_x + room_width - station_size - 3 - 150 - station_size - 5
        else: #j is 3
            station_x = room_start_x + 3

        # A double for loop to draw all 40 squares each of which represent a station
        for i in range(0, total_row):
            station_y = 70 + (station_size + 5)*i
            # location parameters for each square
            para_tuples += (lab_station_index, station_x, station_y, station_size, station_size)

            # use green/grey to mark availability of each station
            if lab_space_info_dict[lab_station_index][0] is True:  # available
                para_tuples += get_color_tuple(GREEN)

            else:
                para_tuples += get_color_tuple(GREY)

            # timing information of each station
            last_update_time_array = lab_space_info_dict[lab_station_index][1].split(':')
            para_tuples += (last_update_time_array[0], last_update_time_array[1], last_update_time_array[2])

            lab_station_index += 1

    # parameters for text
    para_tuples += (room_start_x + room_width + 20, room_start_y + 50)
    para_tuples += (room_start_x + room_width + 20, room_start_y + 150)

    para_tuples += (room_start_x + room_width + 20, room_start_y + 650)
    # embed all parameters into the html string to form a complete html
    res = html % para_tuples
    # print res
    return res

# return a string with 3 chars containing the station number
def get_station_num(num):
    if num < 10:
        return '00' + str(num)
    else:
        return '0' + str(num)


'''

Output example: multiple users logged in

09:21:30 up 39 days, 18:54,  4 users,  load average: 0.26, 0.07, 0.06
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
mehrdadh :0       :0               25May16 ?xdm?   2:23m  8.91s gdm-session-worker [pam/gdm-password]
hamreb   :1       :1               28May16 ?xdm?   2:23m  7.51s gdm-session-worker [pam/gdm-password]
hamreb   pts/0    :1               28May16 31days  0.00s  0.00s bash
yw28     pts/1    199-255-210-109. 09:21    2.00s  0.02s  0.00s w

Output example: no user logged in

09:21:30 up 39 days, 18:54,  4 users,  load average: 0.26, 0.07, 0.06
USER     TTY      FROM             LOGIN@   IDLE   JCPU   PCPU WHAT
yw28     pts/1    199-255-210-109. 09:21    2.00s  0.02s  0.00s w

'''

# return True if the lab station is available, and false otherwise
def analyze_output(output):
    array_line = output.split('\n')
    output_len = len(array_line)
    if output_len < 2:
        print 'Not available'
        return False

    if DEFAUT_OUT_LINE in array_line[1]:
        # print output_len

        # If there is no other users logged in, there should be 3 lines as output.
        # This is not very flexible. Needs to be changed later
        if output_len is 3:
            print 'Available'
            return True  # available
        else:
            print 'Not available'
            return False  # not available
    return False


# SSH function and it will be running in a thread
def ssh_worker():
    while True:
        computer_num = 40
        for i in range(0, computer_num):
            ssh_addr = 'yw28@linux-lab-' + get_station_num(i+1) + '.ee.washington.edu'
            print ssh_addr
            proc = subprocess.Popen(['ssh', ssh_addr, "'w'"], stdout=subprocess.PIPE,)
            out = proc.communicate()
            print out[0]
            lab_space_info_dict[i+1] = [analyze_output(out[0]), get_time_str()]



# main part of the server. It calls generate_html() to generates a HTML file using a global data
# structure keeping track of the current current lab space information
def application (environ, start_response):

    response_body = generate_html(170, 40) # dynamically generate HTML file to render
    content_length = sum([len(s) for s in response_body])
    status = '200 OK'
    response_headers = [
        ('Content-Type', 'text/html'),
        ('Content-Length', str(content_length))
    ]
    start_response(status, response_headers)

    return [response_body]


# server function and it will be running in a thread
def appplication_worker():
    server = make_server('localhost', port_num, application)
    server.serve_forever()


def parse_input_args():
    global port_num
    arg_len = len(sys.argv)
    if arg_len > 2:
        print 'Run this file without any argument or with only the port specified'
        print 'Program is quiting'
        sys.exit()

    if arg_len is 2:
        try:
            port_num = int(sys.argv[1])
        except:
            print 'Invalid port number specified. Argument needs to be an integer greater than...'

    # else: use the default port number

if __name__ == "__main__":

    parse_input_args()

    # 2 threads running. One thread is responsible for sshing into all computers in a serial way
    worker_1 = Thread(target=ssh_worker, args=())
    # If the thread is set to daemon, that means when the main thread terminates, the thread is terminated too
    worker_1.setDaemon(True)
    worker_1.start()

    # Another thread is responsible for running the server
    worker_2 = Thread(target=appplication_worker, args=())
    worker_2.setDaemon(True)
    worker_2.start()

    while True:
        print 'Enter \'q\' to exit'
        usr_input = raw_input()
        print usr_input
        if usr_input == 'q':
            sys.exit()


