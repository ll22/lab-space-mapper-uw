
Before running the application make sure the python version is 2.7.11
Use commands such as "python -V" to check the python version

To run the application:
    python lab_space_mapper.py <port_number>
    <port_number is an optional argument>

TODO:
    1. I used the public SSH key of my own laptop for setup so the SSH command works on my laptop. I don't know how to make it work for anyone who just pulled the code
    2. There must be a better way to check if a lab station is available. I used a simple method to check, and it works so far.
    3. Fix bugs exposed in the future.